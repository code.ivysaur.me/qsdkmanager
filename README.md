# qsdkmanager

![](https://img.shields.io/badge/written%20in-C%2B%2B%20%28Qt%29-blue)

A graphical interface for the Android SDK package manager.

Android SDK 23 and earlier shipped with a graphical package manager. In SDK 24 this was hidden, and in SDK 25 it was permanently removed. This application is a standalone utility that reimplements much of its functionality.

`qsdkmanager` works by parsing the output of `sdkmanager` commands. It is hoped that this will remain a stable interface in future versions of the Android SDK after 25.

## Features

- Add and remove packages
- Switch from Stable to Beta/Dev/Canary channels
- Manage obsolete packages
- Automatically accept software licenses
- Self-contained portable .exe for Windows

## Changelog

2017-04-25 1.0.0
- Initial public release
- [⬇️ qsdkmanager-1.0.0-win32.7z](dist-archive/qsdkmanager-1.0.0-win32.7z) *(4.40 MiB)*
- [⬇️ qsdkmanager-1.0.0-src.tar.bz2](dist-archive/qsdkmanager-1.0.0-src.tar.bz2) *(21.85 KiB)*
- [⬇️ qsdkmanager-1.0.0-linux64-bin.tar.xz](dist-archive/qsdkmanager-1.0.0-linux64-bin.tar.xz) *(37.94 KiB)*

